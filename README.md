# iOSアプリ　配布サイト用API

iOSアプリを社内配布するため、必要なファイルをサーバー（AWS）にアップロードしたり、アプリ情報をやり取りするためのAPIをAWSに立てる

**開発環境**
- Node.js: 6.11.1
- npm: 5.3.0
- serverlessフレームワーク: 1.21.1

**構築されるAWSサービス**
- DynamoDB
- S3
- Lambda
- API Gateway
- IAM Role

**環境構築方法**
- serverlessフレームワークをMacにインストール
- アップロード先のAWSアカウントとserverlessフレームワークを紐付ける
- その後、`sls deploy -v`

※serverlessフレームワークの詳細な説明は[こちら](https://qiita.com/CatDust/items/2ed60677293d98330a99)

## API一覧
### /plist
`POST` plistファイルの作成

### /item
`GET` アプリ情報一覧取得<br>
`POST` アプリの登録<br>
`PUT` アプリの変更<br>
`DELETE` アプリの削除<br>

### /item/{id}
`GET` アプリ情報の取得

### /category
`GET` カテゴリー一覧取得<br>
`POST` カテゴリー登録<br>
`PUT` カテゴリー変更<br>
`DELETE` カテゴリー削除<br>

## 参考：クライアントサイドWeb
iOSアプリをダウンロードするWebサイトのUI

### Webダウンロード画面
<a href="https://imgur.com/hmdmdpf"><img src="https://i.imgur.com/hmdmdpf.png" title="source: imgur.com" width="75%"/></a>

### モバイルでのダウンロード画面
<a href="https://imgur.com/eFRYOUU"><img src="https://i.imgur.com/eFRYOUU.png" title="source: imgur.com" /></a>

### アプリのアップロード画面
<a href="https://imgur.com/bVmAcgz"><img src="https://i.imgur.com/bVmAcgz.png" title="source: imgur.com" width="75%"/></a>

### カテゴリー編集画面
<a href="https://imgur.com/kO5B6b4"><img src="https://i.imgur.com/kO5B6b4.png" title="source: imgur.com" width="75%"/></a>


