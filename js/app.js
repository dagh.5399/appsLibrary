'use strict';

let uuid = require('node-uuid');
let AWS = require('aws-sdk');
const s3 = new AWS.S3({apiVersion: '2006-03-01'});
// var lambda = new AWS.Lambda({apiVersion: '2014-11-11'});
let docClient = new AWS.DynamoDB.DocumentClient();

// 成功時のレスポンス
const createResponse = (statusCode, body) => {
  return {
      statusCode: statusCode,
      headers: {
          "Access-Control-Allow-Origin" : "*" // Required for CORS support to work
        },
      body: JSON.stringify(body)
  }
};

// エラーの時のレスポンス
const createErrorResponse = (statusCode, errMessage) => {
  return {
      isBase64Encoded: false,
      statusCode: statusCode,
      headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin' : '*' // Required for CORS support to work
        },
      body: JSON.stringify({message: errMessage})
  }
};

// 使用するテーブル
const appTable = process.env.DYNAMODB_APPS_MASTER;
let bucket = process.env.S3_FILE_BUCKET;


module.exports.get = (event, context, callback) => {
    
  const params = {
      TableName: appTable
  };
  docClient.scan(params, function(err, data) {
    if (err) {
      console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
      // エラー処理する
      const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
      callback(null, response);
      return;
    }

    if (Object.keys(data).length === 0) {
      // データなし
      const response = createErrorResponse(404, 'Not Found. Data is nothing.');
      callback(null, response);
      return;
    }

    // updatedAtで昇順ソート
    data.Items.sort(function(a, b) {
      if (a.updatedAt > b.updatedAt) return -1;
      if (a.updatedAt < b.updatedAt) return 1;
      return 0;
    });

    callback(null, createResponse(200, data.Items));

  });

}

// 指定したIDのアプリ情報を取得
module.exports.getSpecificApp = (event, context, callback) => {
  const id = event.pathParameters.id ;
  // console.log(id);  
  var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}
  
  if (!id) {queryError('id'); return;}

  const params = {
    TableName: appTable,
    Key:{
        "id": id
    }
  };

  docClient.get(params, function(err, data) {
    if (err) {
        console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
        // エラー処理する
        const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
        callback(null, response);
        return;
    }

    if (Object.keys(data).length === 0) {
      // データなし
      const response = createErrorResponse(404, 'Not Found. Data is nothing.');
      callback(null, response);
      return;
    }

    callback(null, createResponse(200, data.Item));
  });
}


// アプリ新規作成
module.exports.post = (event, context, callback) => {
  const item_data = !event.body ? null : JSON.parse(event.body);
  
  var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}

  // リクエストボディがない場合
  if (!item_data) { queryError(' - '); return; }

  // パラメータ取得
  // アプリ名(必須)
  var title = !item_data.title ? null : item_data.title;
  // アプリ説明
  var description = !item_data.description ? null : item_data.description;
  // IPA
  var ipaFile = !item_data.ipaFile ? ' ' : item_data.ipaFile;
  // PLIST(必須)
  var plistFile = !item_data.plistFile ? null : item_data.plistFile;
  // IMAGE
  var iconFile = item_data.iconFile;
  // カテゴリー(必須)
  var categoryId = !item_data.categoryId ? null : item_data.categoryId;

  // リクエストボディエラー処理
  if (!title) { callback(null, queryError('title')); return; }
  // if (!ipaFile) { callback(null, queryError('ipaFile')); return; }
  if (!plistFile) { callback(null, queryError('plistFile')); return; }
  if (!categoryId) { callback(null, queryError('categoryId')); return; }

  // PLISTファイル作成(appslibrary-dev-uploadApp呼び出し）
// 　let plistParam = {
//     FunctionName: "appslibrary-dev-uploadApp",
//     InvokeArgs: JSON.stringify({ "ipaFile": ipaFile, "appTitle": title })
// }
// lambda.invokeAsync(plistParam, function (err, data) {
//   if(err) {
//     console.log('Lamda error')
//     console.log(JSON.stringify(err, null, 2));
//     const res = createErrorResponse(500, 'LAMBDA ERROR: Failed to create plist file');
//     callback(null, res);
//     return;
//   }

// PLIST作成関数呼び出しを辞めた理由
// - 他のLambda関数を呼び出そうとすると、Payloadをbase64に変換して送らなければならない
// - Payloadのサイズは128KBまで

    // ID
    var id = uuid.v4().split('-').join('');

    // 日付
    var currentDate = getCurrentDate();

    // DB保存
    const params = {
      TableName: appTable,
      Item: {
          "id": id,
          "title": title,
          "appDescription": description ? description : ' ',
          "ipaFile": ipaFile,
          "plistFile": plistFile,
          "iconFile": iconFile ? iconFile : ' ',
          "categoryId": categoryId,
          "createdAt": currentDate,
          "updatedAt": currentDate
      }
    };

    docClient.put(params, function(err, data) {
      if (err) {
          console.log(JSON.stringify(err, null, 2));
          const res = createErrorResponse(500, 'DYNAMODB ERROR: Failed to insert app data');
          callback(null, res);
          return;
      }
      callback(null, createResponse(200, {"id": id}));
  });  

// });

};



module.exports.put = (event, context, callback) => {
  const item_data = !event.body ? null : JSON.parse(event.body);
  
  var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}

  // リクエストボディがない場合
  if (!item_data) { queryError(' - '); return; }

  // パラメータ取得
  // ID(必須)
  var id = !item_data.id ? null : item_data.id;
  // アプリ名
  var title = item_data.title;
  // アプリ説明
  var description = item_data.description;
  // IPA
  var ipaFile = item_data.ipaFile;
  // PLIST
  var plistFile = item_data.plistFile;
  // IMAGE
  var iconFile = item_data.iconFile;
  // カテゴリー
  var categoryId = item_data.categoryId;

  // パラメータエラー
  if (!id) { callback(null, queryError('id')); return; }

  const params = {
    TableName: appTable,
    Key:{
        "id": id
    }
  };

  docClient.get(params, function(err, data) {
    if (err) {
        console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
        // エラー処理する
        const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
        callback(null, response);
        return;
    }

    if (Object.keys(data).length === 0) {
      // データなし
      const response = createErrorResponse(404, 'Not Found. Data is nothing.');
      callback(null, response);
      return;
    }

    // 取得したアプリのデータ
    let appData = data.Item;

    // IPAあれば削除（PLISTも）
    if (ipaFile) {
      // 非同期でS3からIPA削除
      const paramS3IPA = {
          Bucket: bucket,
          Key: appData.ipaFile
      };
      s3.deleteObject(paramS3IPA, function(err, data) {});

      // 非同期でS3からIPA削除
      const paramS3PLIST = {
          Bucket: bucket,
          Key: appData.plistFile
      };
      s3.deleteObject(paramS3PLIST, function(err, data) {});
    }

    // iconFileあれば削除
    if (iconFile) {
      // 非同期でS3からIPA削除
      const paramS3ICON = {
          Bucket: bucket,
          Key: appData.iconFile
      };
      s3.deleteObject(paramS3ICON, function(err, data) {});
    }

    let currentDate = getCurrentDate();

    // DYNAMODB 更新
    const appParams = {
      TableName: appTable,
      Item: {
        'id': id,
        "title": title ? title : appData.title,
        "appDescription": description ? description : appData.appDescription,
        "ipaFile": ipaFile ? ipaFile : appData.ipaFile,
        "plistFile": ipaFile ? ipaFile.replace('.ipa', '.plist') : appData.plistFile,
        "iconFile": iconFile ? iconFile : appData.iconFile,
        "categoryId": categoryId? categoryId : appData.categoryId,
        "createdAt": appData.createdAt,
        "updatedAt": currentDate
      }
    }

    docClient.put(appParams, function(err, data) {
      if (err) {
          console.log(JSON.stringify(err, null, 2));
          const res = createErrorResponse(500, 'DYNAMODB ERROR: Failed to insert app data');
          callback(null, res);
          return;
      }
      callback(null, createResponse(200, {"updatedAt": currentDate}));
    });  

  });


}

module.exports.delete = (event, context, callback) => {
  var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}
  
  let id = event.queryStringParameters.id;
  if (!id) { queryError('id'); return; }

  const params = {
    TableName: appTable,
    Key:{
        "id": id
    }
  };

  docClient.get(params, function(err, data) {
    if (err) {
        console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
        // エラー処理する
        const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
        callback(null, response);
        return;
    }

    if (Object.keys(data).length === 0) {
      // データなし
      const response = createErrorResponse(404, 'Not Found. Data is nothing.');
      callback(null, response);
      return;
    }

    // 取得したアプリのデータ
    let appData = data.Item;

    //IPAファイル削除
    const paramS3IPA = {
      Bucket: bucket,
      Key: appData.ipaFile
    };
    s3.deleteObject(paramS3IPA, function(err, data) {});

    // PLIST 削除
    const paramS3PLIST = {
      Bucket: bucket,
      Key: appData.plistFile
    };
    s3.deleteObject(paramS3PLIST, function(err, data) {});

    // icon 削除
    const paramS3ICON = {
      Bucket: bucket,
      Key: appData.iconFile
    };
    s3.deleteObject(paramS3ICON, function(err, data) {});

    // DYNAMODB 削除
    const params = {
      TableName: appTable,
      Key:{
          "id": id
      }
    };
  
    docClient.delete(params, function(err, data) {
        if (err) {
            console.error('Unable to delete item. Error JSON:', JSON.stringify(err, null, 2));
            // エラー処理する
            const response = createErrorResponse(500, 'Failed to delete item.');
            callback(null, JSON.stringify(response));
            return;
        }

        callback(null, createResponse(200, {}));
    });

  });


}


// 現在の日時を取得
function getCurrentDate() {
  const date = new Date();
  const year = date.getFullYear();
  const month = ('0' + (date.getMonth() + 1)).slice(-2);
  const day = ('0' + date.getDate()).slice(-2);
  const hours = ('0' + date.getHours()).slice(-2);
  const minutes = ('0' + date.getMinutes()).slice(-2);
  const seconds = ('0' + date.getSeconds()).slice(-2);
  const formatDate = year + '/' + month + '/' + day + ' ' + hours + ':' + minutes + ':' + seconds;
  return formatDate
}