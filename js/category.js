'use strict';

let uuid = require('node-uuid');
let AWS = require('aws-sdk');
let docClient = new AWS.DynamoDB.DocumentClient();

// 成功時のレスポンス
const createResponse = (statusCode, body) => {
  return {
      statusCode: statusCode,
      headers: {
          "Access-Control-Allow-Origin" : "*" // Required for CORS support to work
        },
      body: JSON.stringify(body)
  }
};

// エラーの時のレスポンス
const createErrorResponse = (statusCode, errMessage) => {
  return {
      isBase64Encoded: false,
      statusCode: statusCode,
      headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin' : '*' // Required for CORS support to work
        },
      body: JSON.stringify({message: errMessage})
  }
};

// 使用するテーブル
const table = process.env.DYNAMODB_CATEGORY_MASTER;
const apptable = process.env.DYNAMODB_APPS_MASTER;

module.exports.get = (event, context, callback) => {
    
  const params = {
      TableName: table
  };
  docClient.scan(params, function(err, data) {
    if (err) {
      console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
      // エラー処理する
      const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
      callback(null, response);
      return;
    }

    if (Object.keys(data).length === 0) {
      // データなし
      const response = createErrorResponse(404, 'Not Found. Data is nothing.');
      callback(null, response);
      return;
    }

    // updatedAtで昇順ソート
    data.Items.sort(function(a, b) {
      if (a.sequence < b.sequence) return -1;
      if (a.sequence > b.sequence) return 1;
      return 0;
    });

    callback(null, createResponse(200, data.Items));

  });

}



module.exports.post = (event, context, callback) => {
    const item_data = !event.body ? null : JSON.parse(event.body);
    
    var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}
  
    // リクエストボディがない場合
    if (!item_data) { queryError(' - '); return; }
  
    // パラメータ取得
    // カテゴリ名(必須)
    var title = !item_data.title ? null : item_data.title;

    // エラー
    if(!title) { queryError('title'); return; }

    

    const getParams = {
        TableName: table
    }

    docClient.scan(getParams, function(err, data) {
        if (err) {
          console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
          // エラー処理する
          const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
          callback(null, response);
          return;
        }
        
        // ID
        var id = uuid.v4().split('-').join('');
        
        // 日付
        var currentDate = getCurrentDate();

        // シーケンス番号
        let sequence = data.Count;

        // DB保存
        const params = {
            TableName: table,
            Item: {
                "id": id,
                "title": title,
                "sequence": sequence,
                "createdAt": currentDate,
                "updatedAt": currentDate
            }
        };

        docClient.put(params, function(err, data) {
            if (err) {
                console.log(JSON.stringify(err, null, 2));
                const res = createErrorResponse(500, 'DYNAMODB ERROR: Failed to insert app data');
                callback(null, res);
                return;
            }
            callback(null, createResponse(200, {"id": id,"sequence":sequence}));
        });  

    });
}



module.exports.put = (event, context, callback) => {
    const item_data = !event.body ? null : JSON.parse(event.body);
    
    var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}
  
    // リクエストボディがない場合
    if (!item_data) { queryError(' - '); return; }
  
    // パラメータ取得
    // ID(必須)
    var id = !item_data.id ? null : item_data.id;
    var title = !item_data.title ? null : item_data.title;
    var sequence = !item_data.sequence && item_data.sequence !== 0 ? null : item_data.sequence;

    // エラー
    if(!id) { queryError('id'); return; }

    const params = {
        TableName: table,
        Key:{
            "id": id
        }
      };
    
    docClient.get(params, function(err, data) {
        if (err) {
            console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
            // エラー処理する
            const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
            callback(null, response);
            return;
        }
    
        if (Object.keys(data).length === 0) {
          // データなし
          const response = createErrorResponse(404, 'Not Found. Data is nothing.');
          callback(null, response);
          return;
        }

        let item = data.Item;
        let deleteindex = item.sequence;

        // 日付
        var currentDate = getCurrentDate();
        
        // DB保存
        const params = {
            TableName: table,
            Item: {
                "id": id,
                "title": title ? title : item.title,
                "sequence": item.sequence,
                "createdAt": item.createdAt,
                "updatedAt": currentDate
            }
        };

        docClient.put(params, function(err, putData) {
            if (err) {
                console.log(JSON.stringify(err, null, 2));
                const res = createErrorResponse(500, 'DYNAMODB ERROR: Failed to insert app data');
                callback(null, res);
                return;
            }

            // シーケンス番号変更の必要なければ、結果を返す
            if (!sequence && sequence !== 0) {
                callback(null, createResponse(200, {"updatedAt": currentDate}));
                return;
            }

            // シーケンス番号変更処理
            // 全件取得　→　昇順ソート
            const params = {
                TableName: table
            };
            docClient.scan(params, function(err, alldata) {
              if (err) {
                console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
                // エラー処理する
                const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
                callback(null, response);
                return;
              }
          
              if (Object.keys(data).length === 0) {
                // データなし
                const response = createErrorResponse(404, 'Not Found. Data is nothing.');
                callback(null, response);
                return;
              }
          
              // 現在のsequenceで昇順ソート!!
              alldata.Items.sort(function(a, b) {
                if (a.sequence < b.sequence) return -1;
                if (a.sequence > b.sequence) return 1;
                return 0;
              });

              // 変更したいカテゴリーを新しいシーケンス番号の場所に入れる
            var newItem = {
                "id": id,
                "title": title ? title : item.title,
                "sequence": sequence,
                "createdAt": item.createdAt,
                "updatedAt": currentDate
            }
              alldata.Items.splice(deleteindex, 1);
              alldata.Items.splice(sequence, 0, newItem);
              console.log(alldata);

            // sequenceを振り直す
            var requestArray = [];
            alldata.Items.forEach(function(obj, index) {
                obj.sequence = index;
                var reqitem = {
                    PutRequest : {
                        Item: obj
                    }
                };
                requestArray.push(reqitem);
            });

            var tempDic = {};
            tempDic[table] = requestArray;
            var alldataParams = {};
            alldataParams['RequestItems'] = tempDic;


              docClient.batchWrite(alldataParams, function(err, butchdata) {
                if (err) {
                    console.error('Error JSON:', JSON.stringify(err, null, 2));
                    // エラー処理する
                    const response = createErrorResponse(500, 'ERROR Failed to update data.');
                    callback(null, response);
                    return;
                  }
                  callback(null, createResponse(200, {"updatedAt": currentDate}));
              });
            });
        });
    });
}

module.exports.delete = (event, context, callback) => {
    var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}
    
    let id = event.queryStringParameters.id;
    if (!id) { queryError('id'); return; }
  
    // シーケンス番号変更処理
    // 全件取得　→　昇順ソート
    const params = {
        TableName: table
    };
    docClient.scan(params, function(err, alldata) {
        if (err) {
        console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
        // エラー処理する
        const response = createErrorResponse(500, 'ERROR Failed to fetch data from DYNAMODB.');
        callback(null, response);
        return;
        }
          
        if (Object.keys(alldata).length === 0) {
        // データなし
        const response = createErrorResponse(404, 'Not Found. Data is nothing.');
        callback(null, response);
        return;
        }
          
        // 現在のsequenceで昇順ソート!!
        alldata.Items.sort(function(a, b) {
            if (a.sequence < b.sequence) return -1;
            if (a.sequence > b.sequence) return 1;
            return 0;
        });

        // 削除対象を配列から取り除く
        alldata.Items.forEach(function(obj, index) {
            if (obj.id === id) {
                alldata.Items.splice(index, 1);
            }
        });

        // 削除後の配列のsequenceの値を振り直す
        var requestArray = [];
        alldata.Items.forEach(function(obj, index) {
            obj.sequence = index;
            var reqitem = {
                PutRequest : {
                    Item: obj
                }
            };
            requestArray.push(reqitem);
        });

        // sequence振り直した後のデータをbatchWriteの形に整形
        var tempDic = {};
        tempDic[table] = requestArray;
        var alldataParams = {};
        alldataParams['RequestItems'] = tempDic;


        docClient.batchWrite(alldataParams, function(err, butchdata) {
            if (err) {
                console.error('Error JSON:', JSON.stringify(err, null, 2));
                // エラー処理する
                const response = createErrorResponse(500, 'ERROR Failed to update data.');
                callback(null, response);
                return;
            }

            // appsに含まれるカテゴリーIDで一致するものがあれば削除
            const appparams = {
                TableName: apptable
            };
            docClient.scan(appparams, function(err, appdata) {
                // エラー, コンソールだけ
                if (err) {
                    console.error('Unable to get item. Error JSON:', JSON.stringify(err, null, 2));
                    return;
                }
                // データなし, そのまま
                if (Object.keys(appdata).length === 0) { return; }
        
                // 削除するカテゴリーに属したアプリがあれば、そのカテゴリーを削除（上書き）
                appdata.Items.forEach(function(obj, index) {
                    if (obj.categoryId === id) {
                        delete obj.categoryId;
                        const p = {
                            TableName: apptable,
                            Item: obj
                        };
                        // 上書き
                        docClient.put(p, function(err, data) {});  
                    }
                });
            });

            // カテゴリーの削除
            const delparams = {
                TableName: table,
                Key:{ "id": id }
            };
            docClient.delete(delparams, function(err, data) {
                if (err) {
                    console.error('Unable to delete item. Error JSON:', JSON.stringify(err, null, 2));
                    // エラー処理する
                    const response = createErrorResponse(500, 'Failed to delete category');
                    callback(null, JSON.stringify(response));
                    return;
                }
                callback(null, createResponse(200, {}));
            });
        });
    });
}



// 現在の日時を取得
function getCurrentDate() {
    const date = new Date();
    const year = date.getFullYear();
    const month = ('0' + (date.getMonth() + 1)).slice(-2);
    const day = ('0' + date.getDate()).slice(-2);
    const hours = ('0' + date.getHours()).slice(-2);
    const minutes = ('0' + date.getMinutes()).slice(-2);
    const seconds = ('0' + date.getSeconds()).slice(-2);
    const formatDate = year + '/' + month + '/' + day + ' ' + hours + ':' + minutes + ':' + seconds;
    return formatDate
}