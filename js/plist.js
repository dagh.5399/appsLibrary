'use strict';

let AWS = require('aws-sdk');
const s3 = new AWS.S3({apiVersion: '2006-03-01'});
const bucket = process.env.S3_FILE_BUCKET;

const createResponse = (statusCode, body) => {
  return {
      statusCode: statusCode,
      headers: {
          "Access-Control-Allow-Origin" : "*" // Required for CORS support to work
        },
      body: JSON.stringify(body)
  }
};

const createErrorResponse = (statusCode, errMessage) => {
  return {
      isBase64Encoded: false,
      statusCode: statusCode,
      headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin' : '*' // Required for CORS support to work
        },
      body: JSON.stringify({message: errMessage})
  }
};

module.exports.post = (event, context, callback) => {
    const item_data = !event.body ? null : JSON.parse(event.body);
    
    var queryError = function (param) {return createErrorResponse(400, 'Bad Request. query parameter['+param+'] is empty.')}
  
    // リクエストボディがない場合
    if (!item_data) { queryError(' - ') }

    // パラメータ取得
    // ipaファイル名(必須)
    var ipaFile = !item_data || !item_data.ipaFile ? null : item_data.ipaFile;
    // アプリ名(必須)
    var appTitle = !item_data || !item_data.appTitle ? null : item_data.appTitle;

    // ない場合エラー
    if (!ipaFile) { callback(null, queryError('ipaFile')); return; }
    if (!appTitle) { callback(null, queryError('appTitle')); return; }

    // 'https://s3-ap-northeast-1.amazonaws.com/aplb-filebucket/'
    var s3IPAUrl = 'https://s3-ap-northeast-1.amazonaws.com/' + bucket + '/' + ipaFile;
    let fileName = ipaFile.replace('.ipa', '');

    let plistFormat = '<?xml version="1.0" encoding="UTF-8"?>\
    <!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">\
    <plist version="1.0">\
    <dict>\
        <key>items</key>\
        <array>\
            <dict>\
                <key>assets</key>\
                <array>\
                    <dict>\
                        <key>kind</key>\
                        <string>software-package</string>\
                        <key>url</key>\
                        <string>'+ s3IPAUrl +'</string>\
                    </dict>\
                </array>\
                <key>metadata</key>\
                <dict>\
                    <key>bundle-identifier</key>\
                    <string>jp.co.njc.'+fileName+'</string>\
                    <key>bundle-version</key>\
                    <string>1.0.0</string>\
                    <key>kind</key>\
                    <string>software</string>\
                    <key>title</key>\
                    <string>'+appTitle+'</string>\
                </dict>\
            </dict>\
        </array>\
    </dict>\
    </plist>'

    const paramS3 = {
        Bucket: bucket,
        Key: fileName + ".plist",
        Body: plistFormat
    };
    s3.putObject(paramS3, function(err, data) {
        if (err) {
            console.error('Insert S3 failure. Error JSON:', JSON.stringify(err, null, 2));
            callback(null, createErrorResponse(500, 'Insert S3 failure.'));
        } else {
            console.log('Insert S3 succeeded:', JSON.stringify(data, null, 2));
            callback(null, createResponse(200, {'fileName': fileName + '.plist'}));
        }
    });
}